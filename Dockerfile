FROM scratch
LABEL maintainer="JMiahMan <JMiahMan@unity-linux.org>"
ENV DISTTAG=openmamba FGC=mamba FBR=mamba
ADD rootfs-x86_64.tar.xz /
CMD ["/bin/bash"]
